# frozen_string_literal: true

require_relative 'milestone'

module Triage
  class Issue
    attr_reader :attributes

    def initialize(attributes)
      @attributes = attributes
    end

    def milestone
      return nil unless assigned_to_a_milestone?

      @milestone ||= Triage::Milestone.new(milestone_from_api.to_h)
    end

    def labels
      attributes['labels']
    end

    private

    def milestone_from_api
      @milestone_from_api ||=
        if assigned_with_group_milestone?
          Triage.api_client.group_milestone(milestone_group_id, milestone_id)
        else
          Triage.api_client.milestone(milestone_project_id, milestone_id)
        end
    end

    def assigned_with_group_milestone?
      !milestone_group_id.nil?
    end

    def milestone_group_id
      attributes.dig('milestone', 'group_id')
    end

    def milestone_project_id
      attributes.dig('milestone', 'project_id')
    end

    def milestone_id
      attributes.dig('milestone', 'id')
    end

    def assigned_to_a_milestone?
      !attributes['milestone'].nil?
    end
  end
end
