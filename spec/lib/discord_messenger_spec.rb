# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/discord_messenger'

RSpec.describe DiscordMessenger do
  let(:webhook_path) { nil }

  subject { described_class.new(webhook_path) }

  describe '#send_message' do
    context 'when the provided webhook path is nil' do
      # rubocop:disable RSpec/NoExpectationExample
      it 'does not send any API request' do
        # If a request is made, this will fail as we haven't stubbed it!
        subject.send_message('test')
      end
      # rubocop:enable RSpec/NoExpectationExample
    end

    context 'when the provided webhook path is not nil' do
      let(:webhook_path) { 'test_test' }

      it 'sends the API request' do
        message = 'test'

        request = stub_request(:post, "#{DiscordMessenger::BASE_URL}/#{webhook_path}").with(body: hash_including('content' => message))
        subject.send_message(message)

        expect(request).to have_been_requested
      end
    end
  end
end
